{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab 10 : Expansion Rate of the Universe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Objectives\n",
    "\n",
    "The 2011 Nobel prize in physics was awarded to Saul Perlmutter, Brian P. Schmidt, and Adam G. Riess \"*for the discovery of the accelerating expansion of the Universe through observations of distant supernovae*\". **In this lab we will perform our own analysis of a compilation of supernova data.** As always there will be a few things we need to know before we look at the data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialization\n",
    "\n",
    "As always, initialize your environment now by loading all modules required and setting up the plotting environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "74650c7f2511d8576040639cef8d41e2",
     "grade": true,
     "grade_id": "cell-6c62756e546d87ed",
     "locked": false,
     "points": 0,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import scipy.optimize as opt\n",
    "import scipy.interpolate as interp\n",
    "import scipy.integrate as integ\n",
    "import scipy.special as sf\n",
    "import scipy.linalg as la\n",
    "import urllib.request\n",
    "import matplotlib as mpl\n",
    "mpl.rc('xtick', direction='in', top=True)\n",
    "mpl.rc('ytick', direction='in', right=True)\n",
    "mpl.rc('xtick.minor', visible=True)\n",
    "mpl.rc('ytick.minor', visible=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data\n",
    "\n",
    "As noted in the prelab, the data we will use is from the Supernova Cosmology Project, in particular the Union 2.1 set. It is stored in a text file that you may access from its original site at\n",
    "http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt\n",
    "\n",
    "Note that we are working with the *real* data provided by the observers!  We can almost just read this file directly from the website using `loadtxt` similar to the technique from previous labs. In this case, however, the first column of the data file contains text, not just numbers, thus we cannot read the complete file into a NumPy array. If you look at this file you will see it contains five columns:\n",
    "1. **first column  :** name of the supernova,\n",
    "2. **second column :** redshift of the supernova ($z$),\n",
    "3. **third column  :** distance modulus ($\\mu$, see below for a discussion of this),\n",
    "4. **fourth column :** uncertainty in the distance modulus, and\n",
    "5. **fifth column  :** we will ignore.\n",
    "\n",
    "As discussed in the prelab, we only need to read in the second through fourth columns.  Fortunately we can do this with `loadtxt` and it is sufficient to use it here. For actual work it would be far better to use [Pandas](https://pandas.pydata.org/) for reading in and processing data. This is a large, powerful package that we, unfortunately, do not have time to explore."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read the data file and store the redshift, distance modulus, and the uncertainty in the distance modulus in arrays. We will use these arrays below. Note that the `unpack` keyword can be used to make unpacking the columns of data slightly easier and it is recommened that you use it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "2d389ea7831dd1676408a0e27a2260e5",
     "grade": true,
     "grade_id": "cell-6ee2ae1c9090f971",
     "locked": false,
     "points": 4,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "url = \"http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt\"\n",
    "cols = (1,2,3)\n",
    "with urllib.request.urlopen(url) as fp :\n",
    "    z, mu, sigma = np.loadtxt(fp, usecols=cols, unpack=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cosmology\n",
    "\n",
    "As we will discover, this data shows that the expansion rate of the Universe is accelerating. To do this we will fit for a few cosmological parameters. The relevant parameters are \n",
    "- $\\Omega_{\\mathrm{de}}$, the fraction of the total energy density in the Universe in the form of dark energy, \n",
    "- $w$, the \"equation of state parameter\" for dark energy, and \n",
    "- $h$, the reduced Hubble constant. \n",
    "\n",
    "The simplest form of dark energy has $w=-1$; this is called a cosmological constant. Dark energy has \"negative pressure\" and causes the expansion rate of the Universe to accelerate, thus a nonzero $\\Omega_{\\mathrm{de}}$ means we live in an accelerating universe (or more precisely, a universe that will have an accelerating expansion after some time). \n",
    "\n",
    "To discover the acceleration we need to relate these parameters to the observations from the data file. To further simplify things we will assume a flat universe. (In other words, no curvature, thus we are assuming the total $\\Omega=1$, this will be enforced by hand in the equations below. Though a common assumption made, it is arguably not a statistically valid choice. See the end of the lab for more discussion.) The equations provided here are only valid for a flat universe. Describing distances in an expanding universe is not as simple as in Euclidean space so leads to more complicated looking equations. We will leave the details to a cosmology course, for our purposes we need to calculate the following quantities.\n",
    "1. **Distance modulus:** The distance modulus, $\\mu$, is a way of measuring the distance to an object in terms of its magnitude. It is given by\n",
    "$$ \\mu(z, \\Omega_{\\mathrm{de}}, w, h) = 5\\log_{10}\\!\\!\\left[ \\frac{D_L(z, \\Omega_{\\mathrm{de}},w,h)}{10\\,\\mathrm{pc}} \\right], $$\n",
    "where $\\mathrm{pc}$ stands for parsec, a unit of distance.\n",
    "2. **Luminosity distance:** There are few distances we can define in an expanding universe, the one relevant for measuring how bright an object appears is the luminosity distance, $D_L$, and is given by \n",
    "$$ D_L(z, \\Omega_{\\mathrm{de}}, w, h) = (1+z) D_H(h) \\int_0^z\\frac{\\mathrm{d}z'}{E(z', \\Omega_{\\mathrm{de}}, w)}.$$\n",
    "Here\n",
    "  1. $D_H$ is the Hubble distance, $D_H(h) = 3000 h^{-1} \\,\\mathrm{Mpc}$ where $1\\,\\mathrm{Mpc}=10^6\\,\\mathrm{pc} = 3.09\\times10^{22} \\,\\mathrm{meters}$, and\n",
    "  2. $E(z, \\Omega_{\\mathrm{de}}, w) = \\sqrt{(1-\\Omega_{\\mathrm{de}})(1+z)^3 + \\Omega_{\\mathrm{de}} (1+z)^{3(1+w)}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Defining Functions\n",
    "\n",
    "To proceed, we will need to implement the equations given above. I recommend you define each of the previous relationships as a separate function. There are a few things to keep in mind when doing so.\n",
    "1. In the distance modulus the logarithm is base 10, not the natural logarithm. Recall that `np.log()` returns the natural logarithm.\n",
    "2. If you define each of the above relationships in the \"usual\" way they will work for an array of redshift values, $z$, **except for the luminosity distance** due to the integral. For this reason I recommend defining a function `dl(z,Omega_de,w=-1.,h=0.7)` as given by the relationship above, then use `np.vectorize` to construct `DL`, as discussed in the prelab. Notice the order I have chosen for the arguments and the default values. This will be useful below and all your functions should be defined in a similar way.\n",
    "3. Since we are integrating $1/E(z,\\Omega_{\\mathrm{de}},w)$ it makes sense to define a function that returns this inverted quantity instead of just $E(z,\\Omega_{\\mathrm{de}},w)$.\n",
    "4. Be careful with units, make sure they are all consistent!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Keeping the above points in mind, define functions that will allow us to calculate the distance modulus for an array of redshift values. I recommend you define the distance modulus with default arguments as\n",
    "```\n",
    "def distance_modulus(z, Omega_de, w=-1.0, h=0.7):\n",
    "```\n",
    "and all other functions with appropriate defaults, as needed. This will be convenient in what follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "a07d9dd97013bc2918752801024c08f4",
     "grade": true,
     "grade_id": "cell-0afceb8bd6019202",
     "locked": false,
     "points": 5,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "def pc2m(x):\n",
    "    \"\"\"\n",
    "    Converts parasecond to meters (returns value in m)\n",
    "    Params:\n",
    "    ----------\n",
    "    x : Distance (in parsecs)\n",
    "    \"\"\"\n",
    "    return x * 3.08567758128e16 #should we round to 3.09?\n",
    "\n",
    "def D_h(h=0.7):\n",
    "    \"\"\" \n",
    "    Returns Hubble Distance (in m)\n",
    "    Params:\n",
    "    -----------\n",
    "    h : Reduced Hubble Constant\n",
    "    \"\"\"\n",
    "    return 3000. / h * pc2m(1e6)\n",
    "\n",
    "def E(z, Omega_de, w=-1.):\n",
    "    \"\"\"\n",
    "    Returns E as given in equation above\n",
    "    Params:\n",
    "    -----------\n",
    "    z : Redshift data of supernova\n",
    "    Omega_de :\n",
    "    w : \n",
    "    \"\"\"\n",
    "    return np.sqrt((1. - Omega_de)*(1. + z)**3. + Omega_de*(1. + z)**(3.*(1. + w)))\n",
    "def E_inv(z, Omega_de, w=-1.):\n",
    "    \"\"\" \n",
    "    Returns inverse of E\n",
    "    Params:\n",
    "    -----------\n",
    "    same as E\n",
    "    \"\"\"\n",
    "    return 1. / E(z, Omega_de, w=w)\n",
    "\n",
    "def dl(z, Omega_de, w=-1., h=0.7):\n",
    "    \"\"\" \n",
    "    return dl not vectorized\n",
    "    \"\"\"\n",
    "    return (1. + z)*D_h(h)*integ.quad(E_inv, 0., z, args=(Omega_de, w))[0]\n",
    "\n",
    "def DL(z, Omega_de, w=-1., h=0.7):\n",
    "    \"\"\" Converting DL into a vectorized form\n",
    "    \"\"\"\n",
    "    return np.vectorize(dl)(z, Omega_de, w=w, h=h)\n",
    "\n",
    "def distance_modulus(z, Omega_de, w=-1., h=0.7):\n",
    "    \"\"\" Calculation of the distance modulus using DL and the equation given above\n",
    "\n",
    "    \"\"\"\n",
    "    return 5.*np.log10(DL(z, Omega_de, w=w, h=h) / pc2m(10.))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample Models\n",
    "\n",
    "The $\\chi^2$ is not just for fitting, it can also be used test models as discussed in the prelab. Here we will consider a few models and test their goodness of fit (find their $p$-values).\n",
    "\n",
    "**For each of these sets of parameters calculate the $\\chi^2$ between the model and the Union data set. Print the $\\chi^2$, the number of degrees of freedom, and the goodness of fit.**\n",
    "\n",
    "*Hint:* Recall that when we calculate the number of degrees of freedom we need to know the number of parameters being fit. Here there is **no fitting** being performed so what do we mean by the number of degrees of freedom?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Matter Dominated\n",
    "\n",
    "In a matter dominated universe there is no dark energy so $\\Omega_{\\mathrm{de}}=0$. Further, let $h=0.7$. Since we are assuming a flat universe, if $\\Omega_{\\mathrm{de}}=0$ then the only other non-neglible component of the universe is matter and we are really considering model with $\\Omega_m=1$ (which is ruled out by other observations, but this is just a test model). Also, since there is no dark energy the value of the $w$ is irrelevant, its value does not enter affect the calculation at all."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "4602a3489438c1aacaad8e239d021097",
     "grade": true,
     "grade_id": "cell-3c8dda411beb5909",
     "locked": false,
     "points": 4,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Matter Dominated Model :\n",
      "Chi^2 = 2123.0905284576697\n",
      "Deg. of Freedom = 580\n",
      "Goodness of Fit = 1.962233248051273e-174\n"
     ]
    }
   ],
   "source": [
    "mu_true = distance_modulus(z, 0) #Omega_de = 0 and is h = .7 (by default)\n",
    "\n",
    "chisq = np.sum(((mu - mu_true)/sigma)**2.)\n",
    "#chisq = np.sum(((y - ymodel(x,A,phi)) / sigma_y)**2)\n",
    "\n",
    "dof = len(z) \n",
    "Q = sf.gammaincc(0.5*dof, 0.5*chisq)\n",
    "print(f\"\"\"Matter Dominated Model :\n",
    "Chi^2 = {chisq}\n",
    "Deg. of Freedom = {dof}\n",
    "Goodness of Fit = {Q}\"\"\")\n",
    "\n",
    "matter_dom = mu_true.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Einstein-deSitter\n",
    "\n",
    "An Einstein-deSitter universe is the opposite of a matter dominated one; all the energy in the universe is a cosmological constant. Use the parameters $\\Omega_{\\mathrm{de}}=1$, $w=-1$, and $h=0.7$. (Of course this model also has problems since $\\Omega_m=0$, i.e., there is no matter in such a universe, which clearly is not consistent with our Universe!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Einstein-deSitter Model:\n",
      "Chi^2 = 1314.1296491701353\n",
      "Deg. of Freedom = 580\n",
      "Goodness of Fit = 7.27577641236153e-59\n"
     ]
    }
   ],
   "source": [
    "mu_true = distance_modulus(z, 1.) #Omega_de = 1  and is h = .7 and w = -1. (both by default)\n",
    "dof = len(z)\n",
    "chisq = np.sum(((mu - mu_true)/sigma)**2.)\n",
    "Q = sf.gammaincc(0.5*dof, 0.5*chisq)\n",
    "print(f\"\"\"Einstein-deSitter Model:\n",
    "Chi^2 = {chisq}\n",
    "Deg. of Freedom = {dof}\n",
    "Goodness of Fit = {Q}\"\"\")\n",
    "Ein_deSit = mu_true.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### CMB (Planck Data Release 3)\n",
    "\n",
    "The fluctuations in the cosmic microwave background (CMB) provide one of the best probes of the fundamental cosmological parameters. The current best observations of the full-sky CMB is from the Planck satellite. One set of parameters derived from their data is $\\Omega_{\\mathrm{de}}=0.6853$, $w=-1$ (assumed for this fit), and $h=0.6732$. A number of simplifications have been made here, see their [Cosmological Parameters paper](https://arxiv.org/abs/1807.06209) for details. These values are extracted from Table 1.\n",
    "\n",
    "*Note:* You should find this is a much better fit than the previous two models, but still not a great one. This suggests there may still be some tension between the CMB and supernova data. This \"tension\" has persisted for a number of years and is an ongoing area of research to understand if it is real and what it would mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "204710e5cfa87e1931a494b67d90b9cd",
     "grade": true,
     "grade_id": "cell-dc22eceb50ed871f",
     "locked": false,
     "points": 2,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CMB data:\n",
      "Chi^2 = 665.7229577287349\n",
      "Deg. of Freedom = 580\n",
      "Goodness of Fit = 0.007718487713566534\n"
     ]
    }
   ],
   "source": [
    "Omega_de = 0.6853\n",
    "h = 0.6732\n",
    "mu_true = distance_modulus(z, Omega_de, h=h)\n",
    "dof = len(z)\n",
    "chisq = np.sum(((mu - mu_true)/sigma)**2.)\n",
    "Q = sf.gammaincc(0.5*dof, 0.5*chisq)\n",
    "print(f\"\"\"CMB data:\n",
    "Chi^2 = {chisq}\n",
    "Deg. of Freedom = {dof}\n",
    "Goodness of Fit = {Q}\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameter Fits\n",
    "\n",
    "We can also fit for the best values of the cosmological parameters, $\\Omega_{\\mathrm{de}}$, $w$, and $h$. We will do this by first fitting for one parameter, then two, then all of them. At each step we should see the value of the $\\chi^2$ decreases (at least by a little) but this does **not** necessarily mean that we have a better fit as shown by the goodness of fit.  In other words, the data may not justify adding more parameters to the model.\n",
    "\n",
    "To get some idea of the expected results, the best fit values from the Supernova Cosmology Project and other cosmological observations are $\\Omega_{\\mathrm{de}}=0.729 \\pm 0.014$ and $w=-1.013 (+0.068/-0.073)$. They do not quote a value for the Hubble constant, $h$, but it is expected to be around $0.7$. These results come from a much more sophisticated analysis than we will perform, does not rely on the assumption of Gaussian errors, and includes both statistical and systematic errors. We will not find exactly the same results but they should be similar. (You can learn the very gory details from their [paper](http://arxiv.org/abs/1105.3470). On going observations, particularly by the SH0ES project, continues to refine these values and they continue to deviate from those derived from the CMB.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Fit for $\\Omega_{\\mathrm{de}}$:\n",
    "\n",
    "Fix $w=-1$ and $h=0.7$. Fit for $\\Omega_{\\mathrm{de}}$. Print the best fit value for $\\Omega_{\\mathrm{de}}$, its uncertainty, the $\\chi^2$ for the fit, the number of degrees of freedom, and the goodness of fit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "92de4a3e1ffdf742fb2e6d270c84ba37",
     "grade": true,
     "grade_id": "cell-373385fcbd22fdb1",
     "locked": false,
     "points": 5,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Param fit 1:\n",
      "Best fit vaule for Omega_de: 0.7202493913428683.\n",
      "Its Uncertainty: 0.01322012241964082\n",
      "Chi^2 = 562.2487721466177\n",
      "Deg. of Freedom = 579\n",
      "Goodness of Fit = 0.6834778371981441\n"
     ]
    }
   ],
   "source": [
    "dof = len(z) - 1\n",
    "(p, C) = opt.curve_fit(distance_modulus, z, mu, sigma=sigma, p0=np.array([0.729]), absolute_sigma=True)\n",
    "Omega_de_solve = p[0]\n",
    "sigma_solve = np.sqrt(C[0][0])\n",
    "chisq = np.sum(((mu - distance_modulus(z, *p))/sigma)**2.)\n",
    "Q = sf.gammaincc(0.5*dof, 0.5*chisq)\n",
    "\n",
    "print(f\"\"\"Param fit 1:\n",
    "Best fit vaule for Omega_de: {Omega_de_solve}.\n",
    "Its Uncertainty: {sigma_solve}\n",
    "Chi^2 = {chisq}\n",
    "Deg. of Freedom = {dof}\n",
    "Goodness of Fit = {Q}\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Fit for $\\Omega_{\\mathrm{de}}$ and $w$:\n",
    "\n",
    "Fix $h=0.7$.  Fit for $\\Omega_{\\mathrm{de}}$ and $w$. Print the best fit values for $\\Omega_{\\mathrm{de}}$ and $w$, their uncertainties, the $\\chi^2$ for the fit, the number of degrees of freedom, and the goodness of fit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "6e054428d60e1904e6bb012ffaa558bb",
     "grade": true,
     "grade_id": "cell-c0fa9543d4ac254f",
     "locked": false,
     "points": 5,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [
    {
     "ename": "NotImplementedError",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNotImplementedError\u001b[0m                       Traceback (most recent call last)",
      "\u001b[1;32m~\\AppData\\Local\\Temp/ipykernel_4360/1067236896.py\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[0;32m      1\u001b[0m \u001b[1;31m# YOUR CODE HERE\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m----> 2\u001b[1;33m \u001b[1;32mraise\u001b[0m \u001b[0mNotImplementedError\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mNotImplementedError\u001b[0m: "
     ]
    }
   ],
   "source": [
    "# YOUR CODE HERE\n",
    "raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Fit for $\\Omega_{\\mathrm{de}}$, $w$, and $h$:\n",
    "\n",
    "Fit for $\\Omega_{\\mathrm{de}}$, $w$, and $h$. Print the best fit values for $\\Omega_{\\mathrm{de}}$, $w$, and $h$, their uncertainties, the $\\chi^2$ for the fit, the number of degrees of freedom, and the goodness of fit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "8daa1bb15ed5dee6075e0ad78d8f037c",
     "grade": true,
     "grade_id": "cell-b5948be8e5073762",
     "locked": false,
     "points": 5,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# YOUR CODE HERE\n",
    "raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Figure\n",
    "\n",
    "Let us put all the results together into a single figure. Plot the data with error bars using `errorbar`. Include lines for the matter dominated and Einstein-deSitter models from above. You should see by eye that these models do not fit the data well.  Include a line from one of the best fit models. (In the final figure do not include the Planck model nor all of the fits as these lines should be indistinguishable.  Feel free to verify this.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "code",
     "checksum": "e86144df13531e0b52df87a9aa0e01cb",
     "grade": true,
     "grade_id": "cell-84a2689c419bd4cd",
     "locked": false,
     "points": 5,
     "schema_version": 3,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "# YOUR CODE HERE\n",
    "raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on the results and figure from above, explain why the awarding of the Nobel prize was justified, that is, why the supernova data requires the expansion rate of our Universe to be accelerating."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "nbgrader": {
     "cell_type": "markdown",
     "checksum": "9f8c3f6c803e19b9f0d5f8259ad0c7ee",
     "grade": true,
     "grade_id": "cell-bf3ce301c08c5e94",
     "locked": false,
     "points": 3,
     "schema_version": 3,
     "solution": true
    }
   },
   "source": [
    "YOUR ANSWER HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Flat Universe?\n",
    "\n",
    "Does it make sense to assume the Universe is flat, that is, can we fix $\\Omega=1$? It is something that is typically done. In fact, the \"standard cosmological model\" is a 6 parameter model that assumes a flat Universe! Despite this, forcing $\\Omega=1$ is a questionable action. Although topology can provide models that must be flat (certain topologies only allow for flat spacetimes), even in these cases we do not expect our observations over a finite region of the Universe to exactly have $\\Omega=1$. It would only be exactly flat if we could measure the entire Universe. Thus it makes much more sense to also keep $\\Omega$ as a free parameter in our fit. This can be done in a way similar to what we have done in this lab; it requires known modifications to the equations we have used. Interestingly, when doing so with CMB data from Planck, the preferred value of the Hubble constant comes out even lower!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Member Participation\n",
    "\n",
    "See Lab00 for instructions on turning in labs. We will follow this procedure the entire semester.\n",
    "\n",
    "In the following cell enter the *Case ID* for each student in the group who partcipated in this lab. Again, see Lab00 for more details. It is expected that you have read and understood those details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Crk97"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "authors": [
   {
    "name": "Craig J Copi",
    "semester": "Spring 2022"
   }
  ],
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
